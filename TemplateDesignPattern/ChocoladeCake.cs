﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TemplateDesignPattern
{
    public class ChocoladeCake : Cake
    { 
        internal override void IngrediëntenVerzamelen()
        {
            Console.WriteLine("Ingrediënten voor chocoladecake verzamelen");
        }

        internal override void CakeVersieren()
        {
            Console.WriteLine("Cake versieren met hagelslag");
        }
    }
}