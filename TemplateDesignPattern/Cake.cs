﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TemplateDesignPattern
{
    public abstract class Cake
    {
        // bij c# moet er geen final bijstaan
        // om in c# te kunnen overriden moet er virtual of abstract bij de method staan
        internal void CakeMaken()
        {
            IngrediëntenVerzamelen();
            IngrediëntenMengen();
            CakeBakken();
            CakeVersieren();  
        }

        internal abstract void IngrediëntenVerzamelen();

        internal void IngrediëntenMengen()
        {
            Console.WriteLine("Ingrediënten mengen");
        }

        internal void CakeBakken()
        {
            Console.WriteLine("Cake in de oven plaatsen en laten bakken");
        }

        internal abstract void CakeVersieren();
    }
}