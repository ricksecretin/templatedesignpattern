﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            ChocoladeCake chocoladeCake = new ChocoladeCake();
            chocoladeCake.CakeMaken();

            Cake gewoneCake = new GewoneCake();
            gewoneCake.CakeMaken();

            Console.ReadLine();
        }
    }
}
