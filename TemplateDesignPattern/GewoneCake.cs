﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TemplateDesignPattern
{
    public class GewoneCake : Cake
    {
        internal override void IngrediëntenVerzamelen()
        {
            Console.WriteLine("Ingrediënten voor gewone cake verzamelen");
        }

        internal override void CakeVersieren()
        {
            Console.WriteLine("Cake versieren met glazuur");
        }
    }
}